# Entrega practica

## Datos

* Nombre: Fernando Real Rojas
* Titulación: GITT + GIAA
* Despliegue (url): http://frealr.pythonanywhere.com/mispalabras/
* Video básico(url): https://www.youtube.com/watch?v=hPyZHCUPUD4
* Vídeo parte opcional(url): https://www.youtube.com/watch?v=ujZldpx3wF0

## Cuenta Admin Site

* admin/admin

## Cuentas usuarios

* fer/gato

## Resumen parte obligatoria

En esta página, MisPalabras, podemos encontrar información acerca de cualquier palabra, además de comentar sobre ellas y añadir información de otros sitios. Se puede consultar cualquier palabra. Si está almacenada, se mostrará la página de esa palabra con toda la información guardada, mientras que si no lo está, se mostrará la definción de Wikipedia y su imagen en esta plataforma. Para poder añadir palabras, información acerca de ellas o comentar es necesario ser un usuario y estar autenticado. Esto lo podemos hacer desde cualquiera de las páginas, bien logueándonos o bien creando una nueva cuenta con usuario y contraseña. También podemos borrar nuestra cuenta si así lo deseamos. En todas las páginas encontraremos una lista de las 10 palabras más votadas y un formulario para buscar una nueva palabra, además de un pie de página con información acerca del número de palabras almacenadas y un ejemplo.
Inicio

En la página de inicio, encontraremos un listado de todas las palabras almacenadas, paginadas de 5 en 5 por orden inverso de creación, mostrándose para cada una de ellas la descripción de Wikipedia, su imagen, el usuario que la almacenó, y su número de votos.
Página de palabra

En la página de una palabra se mostrará su descripción de Wikipedia, la imagen de Wikipedia, la información automática añadida (en caso de que no esté añadida veremos un botón para añadirla si estamos autenticados), consistiendo en la definición de la RAE y la primera imagen de Flickr, el número de votos, los enlaces añadidos y su tarjeta embebida si tienen, un Meme si se ha generado, y los comentarios. En caso de estar autenticados, podemos votar con like o dislike, quitar nuestro voto para esa palabra, añadir enlaces y comentarios, y cambiar el meme.
Página de usuario

Si estamos autenticados, veremos una página de usuario, en la que aparecerán, por orden inverso de creación, todas las aportaciones del usuario a la web, es decir, palabras creadas, comentarios y enlaces añadidos
XML y JSON

También tenemos la opción de pedir un documento en formato XML o JSON. Si lo pedimos en cualquier página, se nos dará un documento con la lista de palabras almacenadas y la información de su página. En cambio, si lo pedimos en la página de una palabra, se nos dará solo esa página en el formato pedido. 

## Lista partes opcionales

* Favicon: inclusión de favicon con imagen y texto
* Botón de like y dislike, y quitar boto
* JSON y XML de una sola palabra, si le damos al botón de JSON o XML en la página de la palabra
* Eliminar cuenta, eliminando con ella todas las acciones del usuario