import datetime
import json
from itertools import chain
import operator
import urllib
import random

import cloudscraper
from urllib.request import urlopen
from xml.etree.ElementTree import parse
from django.contrib.auth.models import User
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
from django.contrib.auth import logout, login, authenticate
from bs4 import BeautifulSoup
from .models import Palabra, Voto, Comentario, Enlace
from django.core import serializers

def contar_palabras():
    return Palabra.objects.all().__len__()

def get_random_pal():
    lista = Palabra.objects.all().values_list('valor')
    if lista.__len__() != 0:
        return str(random.choice(lista)).split("'")[1]
    else:
        return ""

def top_voted():
    palabras = Palabra.objects.all()
    lista_palabras = {}
    for palabra in palabras:
        votos = Voto.objects.filter(palabra=palabra)
        nvotos = 0
        for voto in votos:
            nvotos += voto.valor
        lista_palabras[palabra.valor] = nvotos
    lista_ordenada = sorted(lista_palabras.items(), key=operator.itemgetter(1), reverse=True)[0:10]
    lista_final = []
    for palabra in lista_ordenada:
        lista_final.append(palabra[0])
    return lista_final

def recent_palabras(request,page):
    try:
        lista_final = []
        palabras = Palabra.objects.all().values()
        listaord = (sorted(palabras, key=lambda x: x['fecha'], reverse=True))
        for pal in listaord:
            pal2 = Palabra.objects.get(valor=pal['valor'])
            votos = Voto.objects.filter(palabra=pal2).values()
            nvotos = 0
            for voto in votos:
                nvotos += voto['valor']
            dic = {}
            dic['palabra'] = pal2
            dic['votos'] = nvotos
            lista_final.append(dic)
        return lista_final[5*(page-1):5*(page-1)+5]
    except:
        lista = []
        return lista

def DRAE(request,palabra):
    url = "https://dle.rae.es/" + palabra
    user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
    headers = {'User-Agent': user_agent}
    req = urllib.request.Request(url, headers=headers)
    with urllib.request.urlopen(req) as response:
        html = response.read().decode('utf-8')
    soup = BeautifulSoup(html, 'html.parser')
    definicion = soup.find("meta", {"property": "og:description"})
    if not definicion["content"].startswith("1"):
        return "Esta palabra no está en la RAE"
    return definicion["content"]

def get_flickr(request,palabra):
    try:
        url = "https://www.flickr.com/services/feeds/photos_public.gne?tags=" + palabra
        scraper = cloudscraper.create_scraper()
        soup = BeautifulSoup(scraper.get(url).text, 'html.parser')
        urlimagen = soup.find('feed').findAll('entry')[0]
        for a in urlimagen.findAll('link', {'rel': 'enclosure'}, href=True):
            link = a['href']
        return link
    except:
        return "https://apimeme.com/meme?meme=Grandma-Finds-The-Internet&top=Imagen+no+encontrada+en+Flickr&bottom=a+que+voy+yo+y+la+encuentro"


def search_image(request,palabra):
    try:
        url = "https://es.wikipedia.org/w/api.php?action=query&titles=" + palabra + "&prop=pageimages&format=json&pithumbsize=200"
        query = str(urlopen(url).read().decode('utf-8'))
        objeto = json.loads(query)
        url_media = objeto['query']['pages']
        id = str(url_media).split("'")[1]
        url_imagen = url_media[id]['thumbnail']['source']
    except:
        url_imagen = "https://apimeme.com/meme?meme=Grandma-Finds-The-Internet&top=Imagen+no+encontrada+en+wikipedia&bottom=a+que+voy+yo+y+la+encuentro"
    return url_imagen

def search_palabra(request,palabra):
    try:
        url = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles=" + palabra + "&prop=extracts&exintro&explaintext"
        xmldoc = parse(urlopen(url))
        definicion = xmldoc.find('query/pages/page/extract').text
        if not definicion:
            definicion = "Esta palabra no tiene definición en wikipedia"
        return definicion
    except:
        return "Esta palabra no tiene definición en wikipedia"

def tarjeta_embebida(request,url):
    try:
        user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
        headers = {'User-Agent': user_agent}
        req = urllib.request.Request(url, headers=headers)
        with urllib.request.urlopen(req) as response:
            html = response.read().decode('utf-8')
        soup = BeautifulSoup(html, 'html.parser')
        definicion = soup.find("meta", {"property": "og:description"})["content"]
        if definicion is None:
            definicion = soup.find("meta", {"property": "og:title"})["content"]
        if definicion is None:
            definicion = ""
        imagen = soup.find("meta", {"property": "og:image"})["content"]
        if imagen is None:
            imagen = ""
        return definicion,imagen
    except:
        return "",""

def check_forms(request):
    value = ""
    opcion = ""
    texto = ""
    info = ""
    if request.POST['name'] == "login":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
    elif request.POST['name'] == "word":
        value = "/mispalabras/" + request.POST['valor']
    elif request.POST['name'] == "meme":
        opcion = request.POST['opcionmeme']
        texto = request.POST['texto']
    elif request.POST['name'] == "DRAE":
        info = "RAE"
    elif request.POST['name'] == "Flickr":
        info = "Flickr"
    elif request.POST['name'] == "save":
        info = "save"
    elif request.POST['name'] == "Comentario":
        info = "comentario"
    elif request.POST['name'] == "Enlace":
        info = "enlace"
    elif request.POST['name'] == "Like":
        info = "like"
    elif request.POST['name'] == "Dislike":
        info = "dislike"
    elif request.POST['name'] == "rmvote":
        info = "rmvote"
    return value, opcion, texto, info

def delete_user_view(request):
    usuario = request.user
    logout(request)
    usuario.delete()
    return redirect('/mispalabras')

def inicio(request):
    npals = contar_palabras()
    npaginas = npals // 5
    if (npals % 5) != 0:
        npaginas += 1
    if npaginas != 0:
        npags = list(range(1,npaginas+1))

    else:
        npaginas = 1
        npags = list(range(1,2))
    ejemplo = get_random_pal()
    mipagina = False
    page = int(request.GET.get('page', 1))
    if npaginas == 1:
        next_page = 1
        prev_page = 1
    else:
        if page == 1:
            next_page = page+1
            prev_page = page
        elif page == npaginas:
            next_page = page
            prev_page = page-1
        else:
            next_page = page+1
            prev_page = page-1
    lista = recent_palabras(request,page)
    format = request.GET.get('format', None)
    if format:
        if format == "xml":
            data = serializers.serialize("xml", Palabra.objects.all())
            return HttpResponse(data, content_type='text/xml')
        elif format == "json":
            data = serializers.serialize("json",Palabra.objects.all())
            return HttpResponse(data, content_type='text/json')
    if request.method == "POST":
        value = check_forms(request)
        red = value[0]
        if red:
            return redirect(red)
    lista_ordenada = top_voted()
    context = {
        'top_votadas': lista_ordenada,
        'mipagina': mipagina,
        'npalabras': npals,
        'ejemplo': ejemplo,
        'npages': npags,
        'actual': str(page),
        'next': str(next_page),
        'prev': str(prev_page),
        'lista': lista
    }
    return render(request, 'inicio.html', context)

def register(request):
    format = request.GET.get('format', None)
    if format:
        if format == "xml":
            data = serializers.serialize("xml", Palabra.objects.all())
            return HttpResponse(data, content_type='text/xml')
        elif format == "json":
            data = serializers.serialize("json",Palabra.objects.all())
            return HttpResponse(data, content_type='text/json')
    npals = contar_palabras()
    ejemplo = get_random_pal()
    mipagina = False
    if request.method == "POST" and (request.POST['name'] == "register"):
        username = request.POST['username']
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            email = request.POST['email']
            password = request.POST['password']
            newuser = User.objects.create_user(username, email, password)
            login(request,newuser)
            return redirect('/mispalabras')
    template = 'registration.html'
    lista_ordenada = top_voted()
    context = {
        'top_votadas': lista_ordenada,
        'mipagina': mipagina,
        'npalabras': npals,
        'ejemplo': ejemplo
    }
    return render(request,template,context)

def logout_view(request):
    logout(request)
    return redirect('/mispalabras')

def ayuda(request):
    template = 'ayuda.html'
    lista_ordenada = top_voted()
    npals = contar_palabras()
    ejemplo = get_random_pal()
    mipagina = False
    if request.method == "POST":
        value = check_forms(request)
        red = value[0]
        if red:
            return redirect(red)
    format = request.GET.get('format', None)
    if format:
        if format == "xml":
            data = serializers.serialize("xml", Palabra.objects.all())
            return HttpResponse(data, content_type='text/xml')
        elif format == "json":
            data = serializers.serialize("json",Palabra.objects.all())
            return HttpResponse(data, content_type='text/json')
    context = {
                'mipagina': mipagina,
                'npalabras': npals,
                'ejemplo': ejemplo,
                'top_votadas': lista_ordenada}
    return render(request, template, context)
def mipagina(request, user):
    npals = contar_palabras()
    ejemplo = get_random_pal()
    mipagina = True
    template = 'mipagina.html'
    comentarios = Comentario.objects.filter(usuario=request.user).values()
    enlaces = Enlace.objects.filter(usuario=request.user).values()
    palabras = Palabra.objects.filter(autor=request.user).values()
    listatotal = list(chain(palabras, comentarios, enlaces))
    listatotal = (sorted(listatotal, key=lambda x: x['fecha'], reverse=True))
    lista_ordenada = top_voted()
    if request.method == "POST":
        value = check_forms(request)
        red = value[0]
        if red:
            return redirect(red)
    format = request.GET.get('format', None)
    if format:
        if format == "xml":
            data = serializers.serialize("xml", Palabra.objects.all())
            return HttpResponse(data, content_type='text/xml')
        elif format == "json":
            data = serializers.serialize("json",Palabra.objects.all())
            return HttpResponse(data, content_type='text/json')
    context = {'mipagina': mipagina,
                "listatotal": listatotal,
               'npalabras': npals,
               'ejemplo': ejemplo,
               'top_votadas': lista_ordenada}
    return render(request,template,context)

def palabras(request,palabra):
    npals = contar_palabras()
    ejemplo = get_random_pal()
    RAE = ""
    Flickr = ""
    mipagina = False
    try:
        pal = Palabra.objects.get(valor=palabra)
        pal_exists = True
        definicion = pal.definicion
        url_imagen = pal.imagen
        RAE = pal.RAE
        Flickr = pal.Flickr
        format = request.GET.get('format', None)
        if format:
            if format == "xml":
                data = serializers.serialize("xml", Palabra.objects.filter(valor=palabra))
                return HttpResponse(data, content_type='text/xml')
            elif format == "json":
                data = serializers.serialize("json", Palabra.objects.filter(valor=palabra))
                return HttpResponse(data, content_type='text/json')

        if request.method == "POST":
            value = check_forms(request)
            red = value[0]
            opcion = value[1]
            texto = value[2]
            info = value[3]
            if red:
                return redirect(red)
            if opcion:
                pal.Meme = "https://apimeme.com/meme?meme=" + opcion + "&top=" + palabra + "&bottom=" + texto
            if info == "RAE":
                pal.RAE = DRAE(request,palabra)
            elif info == "Flickr":
                pal.Flickr = get_flickr(request,palabra)
            elif info == "comentario":
                comen = request.POST['valor']
                c = Comentario(valor=comen, palabra=pal, usuario=request.user, fecha=datetime.datetime.now())
                c.save()
            elif info == "enlace":
                link = request.POST['valor']
                descripcion, imagen_tarjeta = tarjeta_embebida(request,link)
                if descripcion and imagen_tarjeta:
                    e = Enlace(valor=link,palabra=pal,usuario=request.user,fecha=datetime.datetime.now(),
                               descripcion=descripcion,imagen=imagen_tarjeta)
                elif imagen_tarjeta:
                    e = Enlace(valor=link, palabra=pal, usuario=request.user, fecha=datetime.datetime.now(), imagen=imagen_tarjeta)
                elif descripcion:
                    e = Enlace(valor=link, palabra=pal, usuario=request.user, fecha=datetime.datetime.now(),
                               descripcion=descripcion)
                else:
                    e = Enlace(valor=link, palabra=pal, usuario=request.user, fecha=datetime.datetime.now())
                e.save()
            elif info == "like":
                valor = 1
                v = Voto(valor=valor,palabra=pal,usuario=request.user,fecha=datetime.datetime.now())
                v.save()
            elif info == "dislike":
                valor = -1
                v = Voto(valor=valor,palabra=pal,usuario=request.user,fecha=datetime.datetime.now())
                v.save()
            elif info == "rmvote":
                v = Voto.objects.get(usuario=request.user,palabra=pal)
                v.delete()
            pal.save()
            return redirect('/mispalabras/' + palabra)
        if pal.RAE != "":
            RAE_saved = True
        else:
            RAE_saved = False
        if pal.Flickr != "":
            Flickr_saved = True
        else:
            Flickr_saved = False
        Meme = pal.Meme
        votos = Voto.objects.filter(palabra=pal)
        user_voted = False
        if request.user.is_authenticated:
            try:
                Voto.objects.get(usuario=request.user,palabra=pal)
                user_voted = True
            except Voto.DoesNotExist:
                pass
        nvotos = 0
        for vo in votos:
            nvotos += vo.valor
        comentarios_list = Comentario.objects.filter(palabra=pal)
        enlaces_list = Enlace.objects.filter(palabra=pal)
    except Palabra.DoesNotExist:
        nvotos = 0
        user_voted = False
        pal_exists = False
        comentarios_list = []
        enlaces_list = []
        definicion = search_palabra(request,palabra)
        url_imagen = search_image(request,palabra)
        Meme = ""
        RAE_saved = False
        Flickr_saved = False
        if request.method == "POST":
            value = check_forms(request)
            red = value[0]
            info = value[3]
            if red:
                return redirect(red)
            if info == "save":
                pal = Palabra(valor=palabra, autor=request.user, fecha=datetime.datetime.now(), definicion=definicion,
                              imagen=url_imagen)
                pal.save()
                return redirect('/mispalabras/' + palabra)
    lista_ordenada = top_voted()
    context = {
        'Flickr': Flickr,
        'RAE': RAE,
        'word_image': url_imagen,
        'definition': definicion,
        'word': palabra,
        'word_exist': pal_exists,
        'imagen_meme': Meme,
        'RAE_saved': RAE_saved,
        'Flickr_saved': Flickr_saved,
        'comentario_list': comentarios_list,
        'enlace_list': enlaces_list,
        'nvotos': nvotos,
        'user_voted': user_voted,
        'top_votadas': lista_ordenada,
        'npalabras': npals,
        'ejemplo': ejemplo
    }
    return render(request,  "palabra.html", context)