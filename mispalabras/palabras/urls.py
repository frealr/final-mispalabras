from django.urls import path
from . import views

urlpatterns = [
    path('ayuda',views.ayuda),
    path('register',views.register),
    path('logout', views.logout_view),
    path('delete', views.delete_user_view),
    path('',views.inicio),
    path('users/<str:user>',views.mipagina),
    path('<str:palabra>',views.palabras)
]