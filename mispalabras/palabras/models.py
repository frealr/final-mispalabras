from django.db import models
from django.contrib.auth.models import User


class Palabra(models.Model):
    tipo = models.TextField(default="Palabra")
    valor = models.TextField()
    definicion = models.TextField()
    imagen = models.URLField()
    autor = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    RAE = models.TextField(default="")
    Flickr = models.URLField(default="")
    Meme = models.URLField(default="")
    fecha = models.DateTimeField()


class Comentario(models.Model):
    tipo = models.TextField(default="Comentario")
    palabra = models.ForeignKey(Palabra, on_delete=models.CASCADE)
    valor = models.TextField(blank=False)
    fecha = models.DateTimeField()
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)


class Enlace(models.Model):
    descripcion = models.TextField(default="")
    imagen = models.URLField(blank=False)
    tipo = models.TextField(default="Enlace")
    palabra = models.ForeignKey(Palabra, on_delete=models.CASCADE)
    valor = models.URLField(blank=False)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    fecha = models.DateTimeField()


class Voto(models.Model):
    tipo = models.TextField(default="Voto")
    palabra = models.ForeignKey(Palabra, on_delete=models.CASCADE)
    valor = models.IntegerField(default=0)
    fecha = models.DateTimeField()
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
