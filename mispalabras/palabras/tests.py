from django.test import TestCase

from django.contrib.auth.models import User


class SearchPalabra(TestCase):

    def test_GET_inicio(self):
        response = self.client.get('/mispalabras/')
        self.assertEqual(response.status_code, 200)

    def test_buscar_palabra_post(self):
        response = self.client.post('/mispalabras/', {'valor': 'perro', 'name': 'word'})
        self.assertEqual(response.url, '/mispalabras/perro')
        self.assertEqual(response.status_code, 302)

    def test_buscar_palabra_get_noexiste(self):
        self.client.post('/mispalabras/register',
                         {'username': 'usuario', 'password': 'contra', 'name': 'register', 'email': 'hola@gmail.com'})
        self.client.post('/mispalabras/', {'username': 'usuario', 'password': 'contra', 'name': 'login'},
                         follow=True)
        response = self.client.get('/mispalabras/perro')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('Save', content)

    def test_buscar_palabra_get_existe(self):
        self.client.post('/mispalabras/register',
                         {'username': 'usuario', 'password': 'contra', 'name': 'register', 'email': 'hola@gmail.com'})
        self.client.post('/mispalabras/', {'username': 'usuario', 'password': 'contra', 'name': 'login'},
                         follow=True)
        self.client.post('/mispalabras/perro', {'name': 'save'})
        response = self.client.get('/mispalabras/perro')
        content = response.content.decode('utf-8')
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('Save', content)

    def test_buscar_ayuda(self):
        response = self.client.get('/mispalabras/ayuda')
        self.assertEqual(response.status_code, 200)

    def test_xml(self):
        response = self.client.get('/mispalabras/?format=xml')
        self.assertEqual(response.status_code, 200)

    def test_json(self):
        response = self.client.get('/mispalabras/?format=json')
        self.assertEqual(response.status_code, 200)


class PalabraTest(TestCase):

    def setUp(self):
        self.client.post('/mispalabras/register',
                         {'username': 'usuario', 'password': 'contra', 'name': 'register', 'email': 'hola@gmail.com'})
        self.client.post('/mispalabras/', {'username': 'usuario', 'password': 'contra', 'name': 'login'},
                         follow=True)
        self.client.post('/mispalabras/perro', {'name': 'save'})

    def test_Comentario(self):
        self.client.post('/mispalabras/perro', {'name': 'Comentario', 'valor': 'Comentario de prueba'})
        response = self.client.get('/mispalabras/perro')
        content = response.content.decode('utf-8')
        self.assertIn('Comentario de prueba', content)
        self.assertEqual(response.status_code, 200)

    def test_Enlace(self):
        self.client.post('/mispalabras/perro', {'name': 'Enlace', 'valor': 'http://www.hola.com'})
        response = self.client.get('/mispalabras/perro')
        content = response.content.decode('utf-8')
        self.assertIn('http://www.hola.com', content)
        self.assertEqual(response.status_code, 200)

    def test_info_info_noesta(self):
        response = self.client.get('/mispalabras/perro')
        content = response.content.decode('utf-8')
        self.assertIn('Obtener definición de la RAE', content)
        self.assertIn('Obtener imagen de Flickr', content)
        self.assertEqual(response.status_code, 200)

    def test_info_Flickr_esta(self):
        self.client.post('/mispalabras/perro', {'name': 'Flickr'})
        response = self.client.get('/mispalabras/perro')
        content = response.content.decode('utf-8')
        self.assertIn('Primera imagen de Flickr', content)
        self.assertEqual(response.status_code, 200)

    def test_info_RAE_esta(self):
        self.client.post('/mispalabras/perro', {'name': 'DRAE'})
        response = self.client.get('/mispalabras/perro')
        content = response.content.decode('utf-8')
        self.assertIn('1.', content)
        self.assertEqual(response.status_code, 200)

    def test_voto_nohecho(self):
        response = self.client.get('/mispalabras/perro')
        content = response.content.decode('utf-8')
        self.assertIn('Votos: 0', content)
        self.assertEqual(response.status_code, 200)

    def test_voto_like(self):
        self.client.post('/mispalabras/perro', {'name': 'Like'})
        response = self.client.get('/mispalabras/perro')
        content = response.content.decode('utf-8')
        self.assertIn('Votos: 1', content)
        self.assertEqual(response.status_code, 200)

    def test_voto_dislike(self):
        self.client.post('/mispalabras/perro', {'name': 'Dislike'})
        response = self.client.get('/mispalabras/perro')
        content = response.content.decode('utf-8')
        self.assertIn('Votos: -1', content)
        self.assertEqual(response.status_code, 200)


class MiPaginaTest(TestCase):

    def setUp(self):
        self.client.post('/mispalabras/register',
                         {'username': 'usuario', 'password': 'contra', 'name': 'register', 'email': 'hola@gmail.com'})
        self.client.post('/mispalabras/', {'username': 'usuario', 'password': 'contra', 'name': 'login'},
                         follow=True)
        self.client.post('/mispalabras/perro', {'name': 'save'})

    def test_Mipagina(self):
        response = self.client.get('/mispalabras/users/usuario')
        self.assertEqual(response.status_code, 200)


class LogInTest(TestCase):

    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret'}
        User.objects.create_user(**self.credentials)
        self.credentials = {
            'username': 'testuser',
            'password': 'secret',
            'name': 'login'}

    def test_login_true(self):
        response = self.client.post('/mispalabras/', self.credentials, follow=True)
        self.assertTrue(response.context['user'].is_active)
        self.assertEqual(response.status_code, 200)

    def test_login_false(self):
        response = self.client.post('/mispalabras/', {'username': 'other', 'password': 'secret', 'name': 'login'},
                                    follow=True)
        self.assertFalse(response.context['user'].is_active)
        self.assertEqual(response.status_code, 200)

    def test_logout(self):
        self.client.post('/mispalabras/', self.credentials, follow=True)
        res1 = self.client.get('/mispalabras/logout')
        response = self.client.get('/mispalabras/')
        self.assertFalse(response.context['user'].is_active)
        self.assertEqual(res1.status_code, 302)

    def test_register(self):
        self.client.post('/mispalabras/register',
                         {'username': 'usuario', 'password': 'contra', 'name': 'register', 'email': 'hola@gmail.com'})
        response = self.client.post('/mispalabras/', {'username': 'usuario', 'password': 'contra', 'name': 'login'},
                                    follow=True)
        self.assertTrue(response.context['user'].is_active)
        self.assertEqual(response.status_code, 200)

    def test_delete(self):
        self.client.post('/mispalabras/', self.credentials, follow=True)
        self.client.get('/mispalabras/delete')
        response = self.client.post('/mispalabras/', self.credentials, follow=True)
        self.assertFalse(response.context['user'].is_active)
        self.assertEqual(response.status_code, 200)
