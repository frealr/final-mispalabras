from django.contrib import admin
from .models import Palabra, Comentario, Enlace, Voto

admin.site.register(Palabra)
admin.site.register(Comentario)
admin.site.register(Enlace)
admin.site.register(Voto)
